﻿
Class MainWindow

#Region "NTC10k"
    Private Sub NupTempNTC10k_ValueChanged(sender As Object, e As RoutedPropertyChangedEventArgs(Of Double?))

        Dim b1 As Double = NupTempNTC10k.Value
        Try
            If NupResistNTC10k Is Nothing Then Exit Sub
            RemoveHandler NupResistNTC10k.ValueChanged, AddressOf NupResistNTC10k_ValueChanged
            NupResistNTC10k.Value = CalcolaResistenzaNTC10k(b1)
            AddHandler NupResistNTC10k.ValueChanged, AddressOf NupResistNTC10k_ValueChanged
        Catch
        End Try
    End Sub

    Private Function CalcolaResistenzaNTC10k(b1 As Double) As Double
        Return 0.000000000000003441099321 * b1 ^ 10 - 0.000000000001243340602 * b1 ^ 9 +
    0.0000000001742948329 * b1 ^ 8 - 0.00000001307391232 * b1 ^ 7 + 0.0000008728259171 * b1 ^ 6 -
    0.0000843880055 * b1 ^ 5 + 0.007621064138 * b1 ^ 4 - 0.5210927166 * b1 ^ 3 + 28.42263994 * b1 ^ 2 -
    1168.310434 * b1 ^ 1 + 27267.07271
    End Function

    Private Sub NupResistNTC10k_ValueChanged(sender As Object, e As EventArgs) Handles NupResistNTC10k.ValueChanged

        If NupResistNTC10k.Value <= NupResistNTC10k.Minimum Or NupResistNTC10k.Value >= NupResistNTC10k.Maximum Then
            Exit Sub
        End If

        If NupResistNTC10k.Value = 10000 Then
            RemoveHandler NupTempNTC10k.ValueChanged, AddressOf NupTempNTC10k_ValueChanged
            NupTempNTC10k.Value = 25
            AddHandler NupTempNTC10k.ValueChanged, AddressOf NupTempNTC10k_ValueChanged
            Exit Sub
        End If

        Dim b1 As Double = 25
        Dim b1_upper As Double = NupTempNTC10k.Maximum
        Dim b1_lower As Double = NupTempNTC10k.Minimum
        Try
            Dim trialResistance As Double = CalcolaResistenzaNTC10k(b1)
            Dim timeout As DateTime = DateTime.Now

            While True
                If trialResistance > NupResistNTC10k.Value * 1.001 Then
                    b1_lower = b1
                    b1 = (b1_lower + b1_upper) / 2
                    trialResistance = CalcolaResistenzaNTC10k(b1)
                ElseIf trialResistance < NupResistNTC10k.Value / 1.001 Then
                    b1_upper = b1
                    b1 = (b1_upper + b1_lower) / 2
                    trialResistance = CalcolaResistenzaNTC10k(b1)
                Else
                    RemoveHandler NupTempNTC10k.ValueChanged, AddressOf NupTempNTC10k_ValueChanged
                    NupTempNTC10k.Value = b1
                    AddHandler NupTempNTC10k.ValueChanged, AddressOf NupTempNTC10k_ValueChanged
                    Exit While
                End If

                If DateDiff(DateInterval.Second, timeout, DateTime.Now) >= 0.5 Then Exit While

            End While


        Catch
        End Try

    End Sub
#End Region
#Region "NTC1k"
    Private Sub NupTempNTC1k_ValueChanged(sender As Object, e As RoutedPropertyChangedEventArgs(Of Double?))
        Dim b1 As Double = NupTempNTC1k.Value
        Try
            RemoveHandler NupResistNTC1k.ValueChanged, AddressOf NupResistNTC1k_ValueChanged
            NupResistNTC1k.Value = CalcolaResistenzaNTC1k(b1)
            AddHandler NupResistNTC1k.ValueChanged, AddressOf NupResistNTC1k_ValueChanged
        Catch
        End Try
    End Sub

    Private Function CalcolaResistenzaNTC1k(a1 As Double) As Double
        Return 0.0000000000000003579483556 * a1 ^ 10 - 0.0000000000001042200122 * a1 ^ 9 +
        0.00000000001170362501 * a1 ^ 8 - 0.0000000007587451925 * a1 ^ 7 + 0.00000005760424709 * a1 ^ 6 -
        0.000006052573491 * a1 ^ 5 + 0.0005284434132 * a1 ^ 4 - 0.03668662104 * a1 ^ 3 + 2.125838818 * a1 ^ 2 -
        94.52507148 * a1 ^ 1 + 2448.799458
    End Function

    Private Sub NupResistNTC1k_ValueChanged(sender As Object, e As EventArgs) Handles NupResistNTC1k.ValueChanged


        If NupResistNTC1k.Value <= NupResistNTC1k.Minimum Or NupResistNTC1k.Value >= NupResistNTC1k.Maximum Then
            Exit Sub
        End If


        Dim b1 As Double = 25
        Dim b1_upper As Double = NupTempNTC1k.Maximum
        Dim b1_lower As Double = NupTempNTC1k.Minimum
        Try
            Dim trialResistance As Double = CalcolaResistenzaNTC1k(b1)
            Dim timeout As DateTime = DateTime.Now

            While True
                If trialResistance > NupResistNTC1k.Value * 1.001 Then
                    b1_lower = b1
                    b1 = (b1_lower + b1_upper) / 2
                    trialResistance = CalcolaResistenzaNTC1k(b1)
                ElseIf trialResistance < NupResistNTC1k.Value / 1.001 Then
                    b1_upper = b1
                    b1 = (b1_upper + b1_lower) / 2
                    trialResistance = CalcolaResistenzaNTC1k(b1)
                Else
                    RemoveHandler NupTempNTC1k.ValueChanged, AddressOf NupTempNTC1k_ValueChanged
                    NupTempNTC1k.Value = b1
                    AddHandler NupTempNTC1k.ValueChanged, AddressOf NupTempNTC1k_ValueChanged
                    Exit While
                End If

                If DateDiff(DateInterval.Second, timeout, DateTime.Now) >= 0.5 Then Exit While

            End While


        Catch
        End Try

    End Sub
#End Region
#Region "PT1k"
    Private Sub NupTempPT1k_ValueChanged(sender As Object, e As RoutedPropertyChangedEventArgs(Of Double?))
        Dim b1 As Double = NupTempPT1k.Value
        Try
            RemoveHandler NupResistPT1k.ValueChanged, AddressOf NupResistPT1k_ValueChanged
            NupResistPT1k.Value = CalcolaResistenzaPT1k(b1)
            AddHandler NupResistPT1k.ValueChanged, AddressOf NupResistPT1k_ValueChanged
        Catch
        End Try
    End Sub

    Private Function CalcolaResistenzaPT1k(a1 As Double) As Double
        Return -0.0006 * a1 ^ 2 + 3.9088 * a1 + 1000
    End Function

    Private Sub NupResistPT1k_ValueChanged(sender As Object, e As EventArgs) Handles NupResistPT1k.ValueChanged


        If NupResistPT1k.Value <= NupResistPT1k.Minimum Or NupResistPT1k.Value >= NupResistPT1k.Maximum Then
            Exit Sub
        End If


        Dim b1 As Double = 0
        Dim b1_upper As Double = NupTempPT1k.Maximum
        Dim b1_lower As Double = NupTempPT1k.Minimum
        Try
            Dim trialResistance As Double = CalcolaResistenzaPT1k(b1)
            Dim timeout As DateTime = DateTime.Now

            While True
                If trialResistance < NupResistPT1k.Value / 1.001 Then
                    b1_lower = b1
                    b1 = (b1_lower + b1_upper) / 2
                    trialResistance = CalcolaResistenzaPT1k(b1)
                ElseIf trialResistance > NupResistPT1k.Value * 1.001 Then
                    b1_upper = b1
                    b1 = (b1_upper + b1_lower) / 2
                    trialResistance = CalcolaResistenzaPT1k(b1)
                Else
                    RemoveHandler NupTempPT1k.ValueChanged, AddressOf NupTempPT1k_ValueChanged
                    NupTempPT1k.Value = b1
                    AddHandler NupTempPT1k.ValueChanged, AddressOf NupTempPT1k_ValueChanged
                    Exit While
                End If

                If DateDiff(DateInterval.Second, timeout, DateTime.Now) >= 0.5 Then Exit While

            End While


        Catch
        End Try

    End Sub
#End Region
#Region "T5"
    Private Sub NupTempT5_ValueChanged(sender As Object, e As RoutedPropertyChangedEventArgs(Of Double?))
        Dim b1 As Double = NupTempT5.Value
        Try
            RemoveHandler NupResistT5.ValueChanged, AddressOf NupResistT5_ValueChanged
            NupResistT5.Value = CalcolaResistenzaT5(b1)
            AddHandler NupResistT5.ValueChanged, AddressOf NupResistT5_ValueChanged
        Catch
        End Try
    End Sub

    Private Function CalcolaResistenzaT5(a1 As Double) As Double
        Return 1854 + (0.00384 * 1854 * a1) + (0.00000494 * 1854 * a1 ^ 2)
    End Function

    Private Sub NupResistT5_ValueChanged(sender As Object, e As EventArgs) Handles NupResistT5.ValueChanged


        If NupResistT5.Value <= NupResistT5.Minimum Or NupResistT5.Value >= NupResistT5.Maximum Then
            Exit Sub
        End If


        Dim b1 As Double = 0
        Dim b1_upper As Double = NupTempT5.Maximum
        Dim b1_lower As Double = NupTempT5.Minimum
        Try
            Dim trialResistance As Double = CalcolaResistenzaT5(b1)
            Dim timeout As DateTime = DateTime.Now

            While True
                If trialResistance < NupResistT5.Value / 1.001 Then
                    b1_lower = b1
                    b1 = (b1_lower + b1_upper) / 2
                    trialResistance = CalcolaResistenzaT5(b1)
                ElseIf trialResistance > NupResistT5.Value * 1.001 Then
                    b1_upper = b1
                    b1 = (b1_upper + b1_lower) / 2
                    trialResistance = CalcolaResistenzaT5(b1)
                Else
                    RemoveHandler NupTempT5.ValueChanged, AddressOf NupTempT5_ValueChanged
                    NupTempT5.Value = b1
                    AddHandler NupTempT5.ValueChanged, AddressOf NupTempT5_ValueChanged
                    Exit While
                End If

                If DateDiff(DateInterval.Second, timeout, DateTime.Now) >= 0.5 Then Exit While

            End While


        Catch
        End Try

    End Sub
#End Region

    Private Sub InfoFlyoutOpen_Click(sender As Object, e As RoutedEventArgs) Handles InfoFlyoutOpen.Click
        Flyout1.IsOpen = True

    End Sub

    Private Sub Form1_Loaded(sender As Object, e As RoutedEventArgs) Handles Form1.Loaded
        Flyout1.Margin = New Thickness(0, 0, 0, 0)
    End Sub
    Private Sub Flyout1_IsOpenChanged(sender As Object, e As RoutedEventArgs) Handles Flyout1.IsOpenChanged

        Me.Topmost = True
        Me.LabelProductName.Content = My.Application.Info.ProductName
        Me.LabelVersion.Content = String.Format("Version {0}", My.Application.Info.Version.ToString)
        Me.LabelCopyright.Content = My.Application.Info.Copyright
        Me.LabelCompanyName.Content = My.Application.Info.CompanyName
        Me.TextBoxDescription.Text = My.Application.Info.Description

    End Sub

End Class
